<?php
/*
Plugin Name: Wp-ftp-media-library-update-by-chau
Plugin URI: http://wordpress.stackexchange.com/questions/74180/upload-images-to-remote-server
Description: Let's you upload images to ftp-server and remove the upload on the local machine.
Version: 0.1
Author: By Chau
Author URI: http://03way.com
*/

/**
 * @version 0.1
 */
 
 
add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'salcode_add_plugin_page_settings_link');
function salcode_add_plugin_page_settings_link( $links ) {
	$links[] = '<a href="' .
		admin_url( 'options-media.php#c_ftp_option_1' ) .
		'">' . __('Settings') . '</a>';
	return $links;
}


 

function myplugin_activate() {

    // Activation code here...
}
register_activation_hook( __FILE__, 'myplugin_activate' );

add_action('admin_init', 'my_general_section');  
function my_general_section() {  
    add_settings_section(  
        'my_settings_section_ftp_media', // Section ID 
        'Option media ftp connect "wp-ftp-media-library"', // Section Title
        'my_section_options_callback', // Callback
        'media' // What Page?  This makes the section show up on the General Settings Page
    );
	

    add_settings_field( // Option 1
        'c_ftp_option_1', // Option ID
        'Ftp cdn', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'media', // Page it will be displayed (General Settings)
        'my_settings_section_ftp_media', // Name of our section
        array( // The $args
            'upload_url_path', // Should match Option ID //nneed = url server
			'placeholder'         => 'https://cdn.imageshow.com/webpbsdf',
        )  
    ); 
	add_settings_field( // Option 1
        'c_ftp_option_4', // Option ID
        'Ftp host', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'media', // Page it will be displayed (General Settings)
        'my_settings_section_ftp_media', // Name of our section
        array( // The $args
            'c_ftp_option_4', // Should match Option ID
			'placeholder'         => '12.12.123.4',
        )  
    ); 

    add_settings_field( // Option 2
        'c_ftp_option_2', // Option ID
        'Ftp account', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'media', // Page it will be displayed
        'my_settings_section_ftp_media', // Name of our section (General Settings)
        array( // The $args
            'c_ftp_option_2', // Should match Option ID			
        )  
    ); 
	
	add_settings_field( // Option 2
        'c_ftp_option_3', // Option ID
        'Ftp password', // Label
        'my_password_callback', // !important - This is where the args go!
        'media', // Page it will be displayed
        'my_settings_section_ftp_media', // Name of our section (General Settings)
        array( // The $args
            'c_ftp_option_3', // Should match Option ID
			
        )  
    ); 
	
	add_settings_field( // Option 1
        'c_ftp_option_5', // Option ID
        'Delete file after upload', // Label
        'my_checkbook_callback', // !important - This is where the args go!
        'media', // Page it will be displayed (General Settings)
        'my_settings_section_ftp_media', // Name of our section
        array( // The $args
            'c_ftp_option_5', // Should match Option ID
			
			
        )  
    ); 

	
    
    register_setting('media','c_ftp_option_2', 'esc_attr');
    register_setting('media','c_ftp_option_3', 'esc_attr');
    register_setting('media','c_ftp_option_4', 'esc_attr');
	register_setting('media','c_ftp_option_1', 'esc_attr');
    register_setting('media','c_ftp_option_5', 'esc_attr');
}

function my_section_options_callback() { // Section Callback
    echo '<p>Config option for ftp acount</p>';  
}

function my_textbox_callback($args) {  // Textbox Callback
    $option = get_option($args[0]);
	
    echo '<input type="text" placeholder="'.$args['placeholder'].'" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" class="regular-text code" />';
}
function my_password_callback($args) {  // Textbox Callback
    $option = get_option($args[0]);
    echo '<input type="password" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" class="regular-text code"/>';
}

function my_checkbook_callback($args) {  // Textbox Callback
    $option = get_option($args[0]);
	//var_dump($option);die;
	
	$is_chekc = '';
	if ( $option == '1' ) {
        $is_chekc = ' checked';
    }
	
	echo "<input type='checkbox' name='".$args[0]."' id='". $args[0] ."' value='1'" .$is_chekc.'/>';
	
    //echo '<input type="password" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" />';
}


function wpse_1504_upload_to_ftp_curl( $args ){
	
	//var_dump($args);die;
	
	
	$upload_dir = wp_upload_dir();
	$upload_url = get_option('upload_url_path');
	$upload_yrm = get_option('uploads_use_yearmonth_folders');
	
	
	$ftp_cdn = get_option('upload_url_path');
	$ftp_acc = get_option('c_ftp_option_2');
	$ftp_pass = get_option('c_ftp_option_3');
	$ftp_host = get_option('c_ftp_option_4');
	$ftp_is_delete_file = get_option('c_ftp_option_5');
	//var_dump($ftp_is_delete_file);die;
	
	$settings = array(
		/*
		'host'	  =>	'194.59.164.2',  			// * the ftp-server hostname
		'port'    =>    21,                                 // * the ftp-server port (of type int)
		'user'	  =>	'u327570649.testftp', 				// * ftp-user
		'pass'	  =>	'hXq`vQ+V',	 				// * ftp-password
		'cdn'     =>    'https://emotionhouse.vn/testftp',			// * This have to be a pointed domain or subdomain to the root of the uploads
		*/
		'host'	  =>	$ftp_host,  			// * the ftp-server hostname
		'port'    =>    21,                                 // * the ftp-server port (of type int)
		'user'	  =>	$ftp_acc, 				// * ftp-user
		'pass'	  =>	$ftp_pass,	 				// * ftp-password
		'cdn'     =>    $ftp_cdn,			// * This have to be a pointed domain or subdomain to the root of the uploads
		
		'path'	  =>	'/',	 					// - ftp-path, default is root (/). Change here and add the dir on the ftp-server,
		'base'	  =>    $upload_dir['basedir']  	// Basedir on local 
	);


	/**
	 * Change the upload url to the ftp-server
	 */

	if( empty( $upload_url ) ) {
		update_option( 'upload_url_path', esc_url( $settings['cdn'] ) );
	}
	
		
	$folder_uplaod_local= $upload_dir['basedir']; //folder uploads local  C:\xampp7\htdocs\wordpress/wp-content/uploads
	$folder_path_mon_day_local= $upload_dir['path']; //folder upload month year  C:\xampp7\htdocs\wordpress/wp-content/uploads/2020/04
	$folder_subdir= $upload_dir['subdir']; ///2020/04
	$folder_subdir = trim($folder_subdir,'/');
	//url folder online
	
	//var_dump($args);die;
	$file_folder_name  = $args['file'];
	//$tem = [];
	//$tem['file_path'] = $folder_uplaod_local.'/'.$file_folder_name;;
	//$tem['file_folder_name'] = $file_folder_name;
	$file_uploads[] = $tem;
	$list_file = $args['sizes'];
	$list_file['full'] = [];
	//$list_file['full']['file'] = trim($file_folder_name,'/'.$folder_subdir);
	$list_file['full']['file'] = str_replace($folder_subdir.'/','',$file_folder_name);
	//var_dump($file_folder_name,$folder_subdir,$list_file['full']['file']);die;
	//var_dump($list_file);die;
	foreach($list_file as $key=>$value){
		$file_tem = $value['file'];
		//$file_uploads[] = $folder_path_mon_day_local.'/'.$file_tem;
		
		$tem = [];
		$tem['file_path'] = $folder_path_mon_day_local.'/'.$file_tem;
		
		$tem['file_folder_name'] = $folder_subdir.'/'.$file_tem;
		$file_uploads[] = $tem;
		
		$info_at = wpse_1504_action_upload_to_curl_ftp($settings['host'],$settings['user'],$settings['pass'],$tem['file_path'],$tem['file_folder_name']);
		//var_dump($tem['file_path'],$tem['file_folder_name']);
		//var_dump($info);//die;
		if($info_at =='' && $ftp_is_delete_file =='1'){
			unlink( $tem['file_path'] );//delete file current
		}
	}
		
	
	
	//var_dump($file_uploads);die;
	return $args;
}
function wpse_1504_action_upload_to_curl_ftp($ftp_host,$ftp_user,$ftp_password,$localfile,$remotefile){
	//var_dump($ftp_host,$ftp_user,$ftp_password,$localfile,$remotefile);
	$ch = curl_init();
	//$localfile = 'a.txt';
	//$remotefile = '2020/03/filename01.txt';
	
	//$localfile = 'a.txt';
	//$remotefile = '2020/03/filename01.txt';
	
	
	//$ftp_user = '';
	//$ftp_password = '';
	//$ftp_host = '';
	$error = 'Cannot get file:'. $localfile;
	
	if(file_exists($localfile)){
		$fp = fopen($localfile, 'r');
		//curl_setopt($ch, CURLOPT_URL, 'ftp://ftp_login:password@ftp.domain.com/'.$remotefile);
		$connectaction = 'ftp://'.$ftp_user.':'.$ftp_password.'@'.$ftp_host.'/'.$remotefile;
		//echo $connectaction;die;
		curl_setopt($ch, CURLOPT_URL, $connectaction);
		curl_setopt($ch, CURLOPT_UPLOAD, 1);
		curl_setopt($ch, CURLOPT_INFILE, $fp);
		curl_setopt($ch, CURLOPT_INFILESIZE, filesize($localfile));
		curl_setopt($ch, CURLOPT_FTP_CREATE_MISSING_DIRS,CURLFTP_CREATE_DIR_RETRY);

		curl_exec ($ch);
		$error_no = curl_errno($ch);
		curl_close ($ch);
		if ($error_no == 0) {
			$error = '';
		} else {
			$error = 'File upload error. '.curl_error($ch);;
			//echo curl_error($ch);			
		}
	}
	

	
	return $error;
}


//function old;
function wpse_74180_upload_to_ftp( $args ) {
	
	//var_dump($args);die;

	$upload_dir = wp_upload_dir();
	$upload_url = get_option('upload_url_path');
	$upload_yrm = get_option('uploads_use_yearmonth_folders');
	
	var_dump($upload_url,$upload_yrm);die;


	/**
	 * Change this to match your server
	 * You only need to change the those with (*)
	 * If marked with (-) its optional 
	 */

	$settings = array(
		'host'	  =>	'194.59.164.2',  			// * the ftp-server hostname
		'port'    =>    21,                                 // * the ftp-server port (of type int)
		'user'	  =>	'u327570649.testftp', 				// * ftp-user
		'pass'	  =>	'hXq`vQ+V',	 				// * ftp-password
		'cdn'     =>    'emotionhouse.vn/testftp',			// * This have to be a pointed domain or subdomain to the root of the uploads
		'path'	  =>	'/',	 					// - ftp-path, default is root (/). Change here and add the dir on the ftp-server,
		'base'	  =>    $upload_dir['basedir']  	// Basedir on local 
	);


	/**
	 * Change the upload url to the ftp-server
	 */

	if( empty( $upload_url ) ) {
		update_option( 'upload_url_path', esc_url( $settings['cdn'] ) );
	}


	/**
	 * Host-connection
	 * Read about it here: http://php.net/manual/en/function.ftp-connect.php
	 */
	
	$connection = ftp_connect( $settings['host'], $settings['port'] );


	/**
	 * Login to ftp
	 * Read about it here: http://php.net/manual/en/function.ftp-login.php
	 */

	$login = ftp_login( $connection, $settings['user'], $settings['pass'] );

	
	/**
	 * Check ftp-connection
	 */

	if ( !$connection || !$login ) {
	    die('Connection attempt failed, Check your settings');
	}


	function ftp_putAll($conn_id, $src_dir, $dst_dir, $created) {
		//var_dump($conn_id, $src_dir, $dst_dir, $created);die;
            $d = dir($src_dir);
	    while($file = $d->read()) { // do this for each file in the directory
	        if ($file != "." && $file != "..") { // to prevent an infinite loop
	            if (is_dir($src_dir."/".$file)) { // do the following if it is a directory
	                if (!@ftp_chdir($conn_id, $dst_dir."/".$file)) {
	                    ftp_mkdir($conn_id, $dst_dir."/".$file); // create directories that do not yet exist
	                }
	                $created  = ftp_putAll($conn_id, $src_dir."/".$file, $dst_dir."/".$file, $created); // recursive part
	            } else {
	                $upload = ftp_put($conn_id, $dst_dir."/".$file, $src_dir."/".$file, FTP_BINARY); // put the files
	                if($upload)
	                	$created[] = $src_dir."/".$file;
	            }
	        }
	    }
	    $d->close();
	    return $created;		
	}

	/**
	 * If we ftp-upload successfully, mark it for deletion
	 * http://php.net/manual/en/function.ftp-put.php
	 */
	$delete = ftp_putAll($connection, $settings['base'], $settings['path'], array());
		

	// Delete all successfully-copied files
	foreach ( $delete as $file ) {
		//unlink( $file );
	}
	
	return $args;
}
add_filter( 'wp_generate_attachment_metadata', 'wpse_1504_upload_to_ftp_curl' );
